/*
 * circular_buffer.c
 *
 *  Created on: 30 A?u 2015
 *      Author: ifyalciner
 */

#include "circular_buffer.h"

Circular_buffer
circular_buffer_create(uint32_t size)
{
	Circular_buffer new_cb = (Circular_buffer)malloc(sizeof(struct Circular_buffer_t));
	new_cb->size  = size + 1; /// We don't let last remaining byte to be written
	new_cb->data  = (uint8_t*)malloc(sizeof(uint8_t) * new_cb->size);
	new_cb->wrIdx = new_cb->rdIdx = 0;
	return new_cb;
}

uint8_t
circular_buffer_write(Circular_buffer cb,
                      uint8_t* data,
                      uint32_t size)
{
	if(size <= circular_buffer_free_size(cb))
	{
		while(size-- > 0)
		{
			cb->data[cb->wrIdx++] = * data++;
			cb->wrIdx %= cb->size;
		}
		return 1;//True
	}
	else
	{
		return 0;//False
	}
}

